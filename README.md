# Girvan-Newman GN Algorithm using Betweenness Centrality for Detecting Communities in Social Network Graphs

Implemented in Python.

My lab assignment in Analysis of Massive Datasets, FER, Zagreb.

Input and output format of program implementing GN algorithm are explained in "in_format.txt" and "out_format.txt" respectively, with 2 concrete in/out examples: "example_small_in.txt"/"example_small_out.txt" and "example_in.txt"/"example_out.txt".
Task descriptions in "TaskSpecification.pdf".

Created: 2021