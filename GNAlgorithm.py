from sys import stdin

def get_parents(num_vertices, v_to_vs_dict, weight_arr, src_v):
    d_list = [float("inf") for i in range(num_vertices)]
    d_list[src_v] = 0

    par_list = [[] for i in range(num_vertices)]
    par_list[src_v] = [None]

    vs_queue = [src_v]

    while len(vs_queue) > 0:
        u = vs_queue.pop(0)
        neigh_vs = v_to_vs_dict[u]

        du = d_list[u]

        for v in neigh_vs:
            new_d = du + weight_arr[u][v]
            if new_d > d_list[v]:
                continue
            elif new_d < d_list[v]:
                vs_queue.append(v)
                d_list[v] = new_d

                par_list[v] = []

                par_list[v].append(u)
            else: # new_d == d_list[v]
                par_list[v].append(u)

    return par_list

def get_all_shortest_paths(i_parents, j, i_to_j_paths, i_to_j_cur_path):

    if i_parents[j] == [None]:
        i_to_j_paths.append(i_to_j_cur_path[:])
        return i_to_j_paths

    j_pars = i_parents[j]

    for j_par in j_pars:
        path_cp = i_to_j_cur_path[:]
        path_cp.append(j)

        get_all_shortest_paths(i_parents, j_par, i_to_j_paths, path_cp)

    return i_to_j_paths


def get_modularity(coms_list, weight_arr, num_vertices):
    two_m = sum([weight_arr[i][j] for i in range(num_vertices) for j in range(num_vertices)])

    Q = 0

    for com in coms_list:

        com_vs = list(com)

        com_len = len(com_vs)

        for i in range(com_len):
            u = com_vs[i]
            ku = sum([weight_arr[u][i1] for i1 in range(num_vertices)])
            for j in range(com_len):
                v = com_vs[j]
                A_uv = weight_arr[u][v]


                kv = sum([weight_arr[v][j1] for j1 in range(num_vertices)])


                Q += (A_uv - (ku * kv) / two_m)


    Q /= two_m

    if (abs(Q) < 1e-5):
        Q = 0

    Q = round(Q, 4)

    return Q




def get_communities(edges_inds, vs_inds):
    coms_list = []
    connected_inds = set()
    for e in edges_inds:
        connected_inds.add(e[0])
        connected_inds.add(e[1])

    for e in edges_inds:
        v1, v2 = e[0], e[1]

        ind_v1_found, ind_v2_found = -1, -1
        for i in range(len(coms_list)):
            com = coms_list[i]
            if v1 in com:
                ind_v1_found = i
            elif v2 in com:
                ind_v2_found = i

        if ind_v1_found != -1 and ind_v2_found != -1:
            coms_list[ind_v1_found] = coms_list[ind_v1_found].union(coms_list[ind_v2_found])
            coms_list.pop(ind_v2_found)
        elif ind_v1_found != -1:
            coms_list[ind_v1_found] = coms_list[ind_v1_found].union({v2})
        elif ind_v2_found != -1:
            coms_list[ind_v2_found] = coms_list[ind_v2_found].union({v1})
        else:
            coms_list.append({v1, v2})

    isolated_inds = vs_inds-connected_inds

    for isol in isolated_inds:
        coms_list.append({isol})

    return coms_list




if __name__=="__main__":
    lines = stdin.readlines()
    i = 0
    line = lines[i]

    edges = []

    v_to_vs_dict = {}
    while len(line) > 0:
        ab = line.split()
        ab = [int(x.strip()) for x in ab]
        edges.append(ab)
        s_v, e_v = ab[0], ab[1]

        vs = v_to_vs_dict.get(s_v, [])
        vs.append(e_v)
        v_to_vs_dict[s_v] = vs

        vs = v_to_vs_dict.get(e_v, [])
        vs.append(s_v)
        v_to_vs_dict[e_v] = vs

        i += 1
        line = lines[i].strip()



    vertices = set()
    vertex_id_to_ind_dict = {}
    vertex_ind_to_id_dict = {}
    vertex_id_to_feat_vect_arr = []


    j = 1
    line = lines[i + 1]
    while len(line) > 0:

        feats = line.split()
        feats = [int(x.strip()) for x in feats]
        vertices.add(feats[0])
        vertex_id_to_feat_vect_arr.append(feats[1:])
        vertex_id_to_ind_dict[feats[0]] = j-1
        vertex_ind_to_id_dict[j-1] = feats[0]

        j += 1
        if (i+j) == len(lines):
            break
        line = lines[i + j]


    v_to_vs_dict_new = {}
    for v in v_to_vs_dict:
        vs_ids = v_to_vs_dict[v]
        v_to_vs_dict_new[vertex_id_to_ind_dict[v]] = [vertex_id_to_ind_dict[vs_id] for vs_id in vs_ids]
    v_to_vs_dict = v_to_vs_dict_new

    edges_inds = [[vertex_id_to_ind_dict[e[0]], vertex_id_to_ind_dict[e[1]]] for e in edges]
    vs_inds = set(vertex_id_to_ind_dict.values())
    coms_list = get_communities(edges_inds, vs_inds)

    isolated_vs_inds_set = vs_inds - v_to_vs_dict.keys()
    for isol_v in isolated_vs_inds_set:
        v_to_vs_dict[isol_v] = []


    vect_repr_len = len(vertex_id_to_feat_vect_arr[0])
    num_vertices = len(vertices)

    weight_arr = [num_vertices * [0] for _ in range(num_vertices)]
    for e in edges:
        v1_id, v2_id = e[0], e[1]
        v1, v2 = vertex_id_to_ind_dict[v1_id], vertex_id_to_ind_dict[v2_id]

        weight_arr[v1][v2] = 1
        weight_arr[v2][v1] = 1



    # max_col_sim = 0
    # for j in range(vect_repr_len):
    #
    #     col = [vertex_id_to_feat_vect_arr[i][j] for i in range(num_vertices)]
    #
    #     max_el_in_col = max(set(col), key=col.count)
    #     col_sim = sum([1 if x == max_el_in_col else 0 for x in col])
    #
    #     max_col_sim = max(max_col_sim, col_sim)

    max_col_sim = vect_repr_len


    feat_vect_len = len(vertex_id_to_feat_vect_arr[0])

    for i in range(num_vertices):
        vi = vertex_id_to_feat_vect_arr[i]
        for j in range(i+1,num_vertices):
            if weight_arr[i][j] == 0:
                continue
            vj = vertex_id_to_feat_vect_arr[j]
            vi_vj_sim = sum([1 if vi[x]==vj[x] else 0 for x in range(feat_vect_len)])
            new_weight = max_col_sim - (vi_vj_sim-1)
            weight_arr[i][j] = new_weight
            weight_arr[j][i] = new_weight

    weight_arr_init = [x[:] for x in weight_arr]

    q_to_coms_dict = {}

    while len(coms_list) < num_vertices:



        v_to_v_shortest_paths_dict = {}
        for i in range(num_vertices-1):

            i_parents = get_parents(num_vertices, v_to_vs_dict, weight_arr, i)

            for j in range(i+1,num_vertices):

                i_to_j_shortest_paths = get_all_shortest_paths(i_parents, j, [], [])
                i_to_j_shortest_paths = [[i]+list(reversed(x)) for x in i_to_j_shortest_paths]

                v_to_v_shortest_paths_dict[(i,j)] = i_to_j_shortest_paths




        edge_centralness_dict = {}
        for ij in v_to_v_shortest_paths_dict:
            if len(v_to_v_shortest_paths_dict[ij]) > 0:
                i, j = ij
                ij_pair_set = set()
                ij_pair_set.add(i)
                ij_pair_set.add(j)
                edge_centralness_dict[frozenset(ij_pair_set)] = 0


        ## UPDATE edge centralness
        for i, j in v_to_v_shortest_paths_dict:

            i_to_j_shortest_paths_list = v_to_v_shortest_paths_dict[(i, j)]
            if len(i_to_j_shortest_paths_list) == 0:
                continue
            N = len(i_to_j_shortest_paths_list)
            add_edge_centralness = 1.0 / N

            for sp_ij in i_to_j_shortest_paths_list:

                sp_len = len(sp_ij)
                for k in range(sp_len-1):
                    v1, v2 = sp_ij[k], sp_ij[k+1]

                    v12_set = frozenset({v1, v2})
                    centralness12_old = edge_centralness_dict[v12_set]
                    centralness12_new = centralness12_old+add_edge_centralness

                    edge_centralness_dict[v12_set] = centralness12_new

        for x in edge_centralness_dict:
            edge_centralness_dict[x] = round((edge_centralness_dict[x] / 2.0), 4)



        max_centralness = max(edge_centralness_dict.values())
        edges_with_max_centralness = [k for k in edge_centralness_dict if edge_centralness_dict[k]==max_centralness]
        edges_with_max_centralness_ids = [[vertex_ind_to_id_dict[y] for y in x] for x in edges_with_max_centralness]
        edges_with_max_centralness_ids = [sorted(x) for x in edges_with_max_centralness_ids]
        edges_with_max_centralness_ids = sorted(edges_with_max_centralness_ids, key=lambda x: (x[0], x[1]))

        modularity = get_modularity(coms_list, weight_arr, num_vertices)
        # print("MODULARITY", modularity)
        for ed in edges_with_max_centralness_ids:
            print("{} {}".format(ed[0],ed[1]))






        coms_list_ids = [{vertex_ind_to_id_dict[ind] for ind in c} for c in coms_list]

        q_to_coms_dict[modularity] = coms_list_ids


        for e in edges_with_max_centralness:
            v1, v2 = e

            # # upd weights
            # weight_arr[v1][v2] = 0
            # weight_arr[v2][v1] = 0

            if [v1, v2] in edges_inds:
                edges_inds.remove([v1, v2])
            elif [v2, v1] in edges_inds:
                edges_inds.remove([v2, v1])

            # upd v to vs dict
            for k in v_to_vs_dict:
                if k == v1:
                    v_to_vs_dict[k].remove(v2)
                elif k == v2:
                    v_to_vs_dict[k].remove(v1)


            # upd centralness
            edge_centralness_dict.pop(e)

            # upd communities set
            coms_list = get_communities(edges_inds, vs_inds)


    best_partition = q_to_coms_dict[max(q_to_coms_dict)]
    best_partition = [sorted(c) for c in best_partition]
    best_partition = sorted(best_partition, key=lambda x: (len(x), x[0]))

    for i in range(len(best_partition)-1):
        bpi = best_partition[i]
        for j in range(len(bpi)-1):
            print(bpi[j], end="-")
        print(bpi[-1], end=" ")
    bpi = best_partition[-1]
    for j in range(len(bpi)-1):
        print(bpi[j], end="-")
    print(bpi[-1])



